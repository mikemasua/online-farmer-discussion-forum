<!DOCTYPE html>
<!doctype html>
<?php
session_start();
include_once("db.php");
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content=" Online Clearance Dashboard">

   <title>Online Clearance</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!--external css-->
    <link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
     <link rel="stylesheet"/>
      <link href="assets/js/gritter/css/jquery.gritter.css" rel="stylesheet"/>
       <link href="assets/lineicons/style.css"" rel="stylesheet"/>
        
      
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet"/>
   
    <link href="assets/css/style-responsive.css" rel="stylesheet"/>
    <link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script language="javascript" type="text/javascript">
    function openWin()
     {
    myWindow = window.open('maindash.php','_self');   // Opens a new window
		}
    </script>
  </head>

  <body>
<section id="container" >
<!-- ******** TOP BAR CONTENT & NOTIFICATIONS ************************ -->
 <!--header start-->
<header class="header black-bg">
<div class="sidebar-toggle-box">
<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
</div>
<!--logo start-->
<a href="maindash.php" class="logo"><b>Online Clearance </b></a>
<!--logo end-->
<div class="nav notify-row" id="top_menu">
<!--  notification start -->
<ul class="nav top-menu">
<!-- settings start -->
<li class="dropdown">
<a data-toggle="dropdown" class="dropdown-toggle" href="gallery.html#">
<i class="fa fa-tasks"></i>
<span class="badge bg-theme">4</span>
</a>
<ul class="dropdown-menu extended tasks-bar">
<div class="notify-arrow notify-arrow-green"></div>
 <li>
<p class="green">The Recent Updates</p>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Admin Panel</div>
<div class="percent">40%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
<span class="sr-only">40% Complete (success)</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Database Update</div>
<div class="percent">60%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
<span class="sr-only">60% Complete (warning)</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Product Development</div>
<div class="percent">80%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
 <span class="sr-only">80% Complete</span>
 </div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Payments Sent</div>
<div class="percent">70%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
<span class="sr-only">70% Complete (Important)</span>
</div>
</div>
</a>
</li>
<li class="external">
<a href="#">See All Updates</a>
</li>
</ul>
</li>
<!-- settings end -->
<!-- inbox dropdown start-->
<li id="header_inbox_bar" class="dropdown">
<a data-toggle="dropdown" class="dropdown-toggle" href="gallery.html#">
<i class="fa fa-envelope-o"></i>
<span class="badge bg-theme">5</span>
</a>
<ul class="dropdown-menu extended inbox">
<div class="notify-arrow notify-arrow-green"></div>
<li>
<p class="green">You have 5 new messages</p>
</li>
<li>
<a href="#">
<span class="photo"><img alt="avatar" src="assets/img/ui-zac.jpg"></span>
 <span class="subject">
<span class="from">Zac Snider</span>
<span class="time">Just now</span>
</span>
<span class="message"> Hi mate, how is everything?</span>
</a>
</li>
<li>
<a href="#">
<span class="photo"><img alt="avatar" src="assets/img/ui-divya.jpg"></span>
<span class="subject">
<span class="from">Divya Manian</span>
<span class="time">40 mins.</span>
</span>
<span class="message">Hi, I need your help with this.</span>
</a>
</li>
<li>
<a href="#">
<span class="photo"><img alt="avatar" src="assets/img/ui-danro.jpg"></span>
<span class="subject">
<span class="from">Dan Rogers</span>
<span class="time">2 hrs.</span>
</span>
<span class="message">Love your new Dashboard.</span>
</a>
</li>
<li>
<a href="maindash.php#">
<span class="photo"><img alt="avatar" src="assets/img/ui-sherman.jpg"></span>
<span class="subject">
<span class="from">Dj Sherman</span>
<span class="time">4 hrs.</span>
</span>
<span class="message">Please, answer asap.</span>
</a>
</li>
<li>
<a href="#">See all messages</a>
</li>
</ul>
</li>
<!-- inbox dropdown end -->
</ul>
<!--  notification end -->
</div>
<div class="top-menu">
<ul class="nav pull-right top-menu">
<li>
<a class="logout" href="logout.php">
<i class="fa fa-sign-out" ></i>
Logout</a></li>
</ul>

</div>
</header>
      <!--header end-->
 
<!-- ********************* MAIN SIDEBAR MENU ************************ -->
      <!--sidebar start-->
<aside>
<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
<ul class="sidebar-menu" id="nav-accordion">
<h3 class="centered">User Profile</h3>             
<p class="centered"><a href="#"><img src="assets/img/elite.jpg" class="img-circle" width="80" height="80"></a></p>
<h5 class="centered"><?php  echo $_SESSION['fname'];?></h5>
<li class="mt">
<a class="active" href="maindash.php#">
<i class="fa fa-dashboard"></i>
<span>Dashboard</span>
 </a>
</li>

<?php
ob_start();
//session_start();
//if (isset($_SESSION['fname'])){

//Register a user
// Get values from form



 $fname=$_SESSION['fname'];
 $get_user= "select * from users where fname='$fname' ";
 $run_user= mysql_query($get_user);
 $row=mysql_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $fname= $row['fname'];
 $user_phone= $row['user_phone'];
 $user_dept= $row['user_dept'];
 $user_reg= $row['user_reg'];
 $user_natid= $row['user_natid'];

echo " 

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-user'></i>
<span><strong> Full Name (s): </strong> $fname </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-bookmark'></i>
<span><strong> National Id: </strong> $user_natid </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-phone'></i>
<span><strong> Telephone: </strong> $user_phone </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-graduation-cap'></i>
<span><strong> Department: </strong> $user_dept </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-desktop'></i>
<span><strong> Reg No: </strong> $user_reg </span>
</a>
</li>
";
 ?>           
 <!-- sidebar menu end-->
</div>
</aside>

<!-- **************  MAIN CONTENT ************************* -->
<!--main content start-->
<section id="main-content">
<section class="wrapper site-min-height">
<h3 class="alert alert-success"><b> <center>DEPARTMENT CLEARANCE FORM 
</center></b> 
</h3>

          	          	<hr> 
<div class="row mt">
<div class="col-lg-12">
<h4 class="mb">Fill and update your info</h4>                     
<form class="form-horizontal style-form" action="" name="validation" method="post" onSubmit="return checkbae()">
<div class="form-panel">
<div class="form-group has-success">
<label class="col-sm-2 col-sm-2 control-label">Full Name(s)</label>
<div class="col-sm-6">                  
<input type="text" class="form-control" name="fname"  value="<?php echo $fname;?>" autofocus id="name1" disabled id="inputSuccess"/>
</div>
</div>
<div class="form-group has-success">
<label class="col-sm-2 col-sm-2 control-label">Registration Number</label>
<div class="col-sm-6">
<input type="text" class="form-control" name="u_reg" placeholder="Enter Admission no." required="required" id="inputSuccess" />
</div>
</div>
<div class="form-group has-success">
<label class="col-sm-2 col-sm-2 control-label">Department</label>
<div class="col-sm-6">
<input type="text" class="form-control" name="u_dept"   autofocus id="name2" required="required" id="inputSuccess"/>
</div>
</div>
<div class="form-group has-success">
<label class="col-sm-2 col-sm-2 control-label">User Course</label>
<div class="col-sm-6">
<input type="text" class="form-control" name="u_class" id="inputSuccess" placeholder="User-Class" autofocus id="name2" required="required"/>
</div>
</div>

<div class="form-group has-success">
<label class="col-sm-2 col-sm-2 control-label">User Course</label>
<div class="col-sm-6">
<select class="form-control" name='u_course' placeholder="Course" id="inputSuccess">
			<option value=''>.....Select Course.....</option>
			<option value='Certificate'><b>Certificate</b></option>
			<option value='Diploma'>Diploma</option>
			<option value='High-Diploma'>High-Diploma</option>
			<option value='Degree'>Degree</option>
			<option value='Masters'>Masters</option>
            <option value='PhD'>PhD</option>
</select>
</div>
</div>           
<button class="btn  btn-success"  name="submit" type="submit" onclick="checkLength()"><i class="fa fa-floppy-o"></i> Save</button>
<button type="button" class="btn btn-success"  data-dismiss="modal" onclick="openWin()"> <i class="fa fa-times-circle-o"></i>Close</button>
</form>
</div>
</section><! --/wrapper -->
</section><!-- /MAIN CONTENT --
<!--main content end-->



<!--footer start-->
      <hr>
<footer class="site-footer">
<div class="text-center">
</div>
</footer>
<!--footer end-->
</section>

    <!-- js placed at the end of the document so the pages load faster -->
	<script src="assets/js/fancybox/jquery.fancybox.js"></script>    
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

 <!--script for this page-->
  
  <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

  </script>
  
  <script>
      //custom select box

      $(function(){
          $("select.styled").customSelect();
      });

  </script>

  <!--main content end-->
<!--footer start-->
<footer class="site-footer">
<div class="text-center">
2017 - Mykdemasua-Chief developer
<a href="maindash.php#" class="go-top">
<i class="fa fa-angle-up"></i>
</a>
</div>
</footer>
<!--footer end-->


    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	

	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    
 
  <?php

//session_start();
//Register a user
// Get values from form

if(!empty($_POST['u_reg']) && !empty($_POST['fname'])  && !empty($_POST['u_class']) && !empty($_POST['u_course'])&& !empty($_POST['u_dept']))
{
    $reg= mysql_real_escape_string($_POST['u_reg']);
	$name= mysql_real_escape_string($_POST['fname']);
	$class=mysql_real_escape_string($_POST['u_class']);
	$course=mysql_real_escape_string($_POST['u_course']);
	$dept=mysql_real_escape_string($_POST['u_dept']);

	$checkusername = mysql_query("SELECT * FROM department WHERE user_reg = '".$reg."'");
      
     if(mysql_num_rows($checkusername) == 1)
     {
	  echo'<script type="text/javascript">alert("Sorry, The Registration Number has already cleared. Please go back and try again.!");</script>';
echo "<script>window.open('maindash.php','_self')</script>";
        //echo "<h1>Error</h1>";
        //echo "<p>Sorry, that username is taken. Please go back and try again.</p>";
     }
     else
     {
$registerquery = mysql_query("INSERT INTO department (user_reg,fname,user_class,user_course,user_dept) VALUES('".$reg."', '".$name."','".$class."', '".$course."','".$dept."')");
        if($registerquery)
        {	
		
echo "<script>alert('Congratulations,You have successfully submitted your clearance..')</script>";
echo "<script>window.open('maindash.php','_self')</script>";
		}
        else 
        {
            echo "<h1>Error</h1>";
            echo "<p>Sorry, your registration failed. Please go back and try again.</p>";    
        }       
}}
exit;
?>  
</body>
</html>
