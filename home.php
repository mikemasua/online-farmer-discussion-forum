<!doctype html>
<?php
session_start();
include("includes/connection.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="styles/home_style.css" media="all" />
<link type="text/css" rel="stylesheet" href="http://cdn.dcodes.net/2/lists/css/dc_lists.css" />
<link rel="shortcut icon" href="images/favicon.png" />
<title>Farmers ChatBox</title>
</head>
<body>

<div class="container">
<div id="head_wrap"><div id="header">
<h4  style="background-color:#4CAF50; height:55px; width:auto; margin-top:7px; -moz-border-radius: 4px; border-radius: 4px;"> Welcome :<?php  echo $_SESSION['user_email'];?></h4>
<ul id="menu">
<li><a href="home.php"> Home</a></li>
<li><a href="members.php"> Members</a></li>
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics))
{
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic.php?topic=$topic_id'>$topic_title </a></li>" ;
}?>
<form action="# " method="get" id="form1">
<input type="text" name="user_query" placeholder="search a topic" required="required" />
<input type="submit" name ="search" value="Search" style="background-color: #4CAF50; border: 2px solid #4CAF50;" />
</form>
</ul>
</div>
</div>
<div class="content">
   <div id="user_timeline">
        <div id="user_details"> 
<?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];
 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 
echo " 
<img src='images/$user_image' width='150' height='150'  />
<p ><strong> Name: </strong> $user_name </p>
<p><strong> Cellphone: </strong> $user_phone </p>
<p><strong> County: </strong> $user_county </p>
<p><strong> Last Login: </strong> $last_login </p>
<p> <a href='messages.php?inbox&u_id=$user_id'> My messages ($count_msg)</a></p>
<p> <a href='my_posts.php?u_id=$user_id'> My Posts ($posts)</a></p>
<p> <a href='edit.php?u_id=$user_id'> Edit Profile</a></p>
<p> <a href='logout.php'>  Logout</a></p>
";
 ?>
</div>
</div> 
<div id="content_timeline">
   <form action="home.php?id= <?php echo $user_id;?>" method="post" id="f">
   <h2 style="background:#4CAF50; color:#FFFFFF; height:40px; width:610px; margin-bottom:5px; padding-top:5px;"  align="center"> Post your Query,Solution,idea Here:</h2>
   
   <input  type="text" name="title"  placeholder="Write  a Title..... " size ="69" required="required" /></br>
   <textarea cols="53 " rows="6" name="content" placeholder ="Write a Description...." ></textarea></br>
   <select name="topic"> 
   <option> ...Select Topic ...</option>
   <?php getTopics();?>
   </select>
   <input style="-webkit-transition-duration:0.4s; margin-top:3px; transition-duration: 0.4s;background-color: #4CAF50; border: 2px solid #4CAF50; " type="submit"    name="sub" value="Post To Timeline">
  </form>
   <?php insertPost();?>
<h3 style="background:#4CAF50; color:#FFFFFF; height:40px; width:610px; margin-bottom:5px; padding-top:5px;"  align="center"> Most Recent Discussions:</h3>
   <?php get_posts();?>
  </div></div></div>
    <div id="userss">
      <h3 style="background:#4CAF50; color:#FFFFFF; height:40px; width:178px; margin-bottom:5px; padding-top:5px;"  align="center"> All Registered Users :</h3>

<?php
getUsers();
?>
</div>
</body>
</body>
</html>
<?php  } ?>