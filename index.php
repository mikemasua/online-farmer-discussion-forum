<?php 
/* Main page with two forms: sign up and log in */
require 'db.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title>Sign-Up/Login Form</title>
<?php include 'css/css.html'; ?>

</head>
<?php 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
if (isset($_POST['login'])) { //user logging in
require 'login.php';
}
 elseif (isset($_POST['register'])) { //user registering
 require 'register1.php';
        
}
}
?>
<div id="banner1">
<ul>
<li> <a href="#"> <h1 style=" color:#4CAF50; margin-bottom:10px; font-family:Baskerville, 'Palatino Linotype', Palatino, 'Century Schoolbook L', 'Times New Roman', serif;" align="center"> <font size="+2">Online Farmers Forum </font></h1> </a></li>

</ul>
</div>
<body>

<div class="form">
<ul class="tab-group">
<li class="tab active"><a href="#login">Log In</a></li>
<li class="tab"><a href="#signup">Sign Up</a></li>
</ul>
<div class="tab-content">
<div id="login">   
<h1 style="color:#4CAF50;">Welcome !</h1>
<form action="index.php" method="post" autocomplete="off">
          
 <div class="field-wrap">
<label>Email Address<span class="req"></label>
<input type="email" required autocomplete="off" name="email" style="border-radius:6px;"/></div>
          
<div class="field-wrap">
<label>Password<span class="req"></label>
<input type="password" required autocomplete="off" name="pass" style="border-radius:6px;"/> </div>
          
<p class="forgot"><a href="#">Forgot Password?</a></p>
          
<button class="button button-block" name="login" />Log In</button>
</form>

</div>
          
<div id="signup">   
          <h1 style="color:#4CAF50;">Sign Up for Free</h1>
          
<form action="index.php" method="post" autocomplete="off">
<div class="field-wrap">
<label>Full Name(s)<span class="req"></span></label>
<input type="text" required autocomplete="off" name="u_name" />
</div> 
<div class="field-wrap">
<label>Email Address<span class="req"></label>
<input type="email"required autocomplete="off" name="u_email" />
</div>      
<div class="top-row">
<div class="field-wrap">
<select   name="u_role" required> 
  <option value="">..Select Occupation...</option>
  <option value="Vet.Officer"> Vet.Officer</option>
  <option value ="Farmer"> Farmer</option>
  <option value ="Agronomist"> Agronomist</option>
  <option value="Agri.Engineer"> Agri.Engineer</option>
  <option value="Water.Engineer"> Water.Engineer</option>
  <option value="Laboaratoligist">Laboaratoligist</option>
  <option value="HumanResource"> HumanResource</option>
  </select>
</div>
</span>
<div class="field-wrap">
<label>Cellphone<span class="req"></span></label>
<input type="number" required autocomplete="off"  onkeypress="return isNumberKey(event)" name="u_phone" />
</div>
</div>

<div class="field-wrap">
<label>Password<span class="req"></label>
<input type="password" required autocomplete="off" name="u_pass" />
</div>
 
<div class="top-row">
<div class="field-wrap">
<select name="u_gender" required><br>
<strong><option value="" >......Select Gender.....</option</strong>
<option value="Male"> Male</option>
<option value="Female"> Female</option>
</strong>
</select>
</div>
        
<div class="field-wrap">
<select   name="u_county" title=".....Select  County......" required>
<strong><option value="" >.....Select  County......</option></strong>
      <option  value="Kitui">Kitui</option>
      <option value="Nairobi">Nairobi</option>
      <option value="Nyandarua">Nyandarua</option>
      <option value="Kajiado">Kajiado</option>
      <option value="Machakos">Machakos</option>
      <option value="Kisii">Kisii</option>
      <option value="Embu">Embu</option>
      <option value="Tharaka">Tharaka</option>
      <option value="Meru">Meru</option>
      <option value="Tana">Tana</option>
      <option value="Kiambu">Kiambu</option>
      <option value="Makueni">Makueni</option>
      <option value="Nyeri">Nyeri</option>
      <option value="Garissa">Garissa</option>
      <option value="Samburu">Samburu</option>
      </select >
</div>
</div>
<button type="submit" class="button button-block" name="register" />Register</button>
</form>
<script>
function isNumberKey(evt){
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
       
return false;
return true;
}
</script>
</div>  
        
</div><!-- tab-content -->
      
</div> <!-- /form -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>
</body>

</html>
