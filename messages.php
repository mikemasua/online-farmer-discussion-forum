<!doctype html>
<?php
session_start();
include("includes/connection.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<html>
<head>
<meta charset="utf-8">

<link rel="stylesheet" href="styles/home_style3.css" media="all" />
<title>Farmers ChatBox</title>
</head>
<body>
<div class="container">
<div id="head_wrap">
<div id="header">
<h4 > Welcome :<?php  echo $_SESSION['user_email'];?></h4>
<ul id="menu">
<li><a href="home.php"> Home</a></li>
<li><a href="members.php"> Members</a></li>
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics)){
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic.php?topic=$topic_id '>$topic_title </a> </li>" ;
	}
?>
<form action="results.php" method="get" id="form1">
<input type="text" name="user_query" placeholder="search a topic" required="required" />
<input type="submit" name ="search" value="Search" />
</form>
</ul>
</div>

<div class="content">

<div id="user_timeline">

<div id="user_details"> 
 
 <?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];
 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
  $posts= mysqli_num_rows($run_posts); 

//getting numer of messages
$select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 
 
echo " 
<img src='images/$user_image' width='200' height='200'  />
<p><strong> Name: </strong> $user_name </p>
<p><strong> Cellphone: </strong> $user_phone </p>
<p><strong> County: </strong> $user_county </p>
<p><strong> Last Login: </strong> $last_login </p>
<p> <a href='messages.php?inbox&u_id=$user_id'> My messages ($count_msg)</a></p>
<p> <a href='my_posts.php?u_id=$user_id'> My Posts ($posts)</a></p>
<p> <a href='edit.php?u_id=$user_id'> Edit Profile</a></p>
<p> <a href='logout.php'> Logout</a></p>
";
 
 ?>
 </div></div>
<div id="userss">
 <h3 style="background:#4CAF50; color:#FFFFFF; height:40px; width:220px; margin-bottom:5px; padding-top:5px;"  align="center"> All Registered Users :</h3>
<?php getUsers();?>
</div></div>
<div id="content_timeline">
<h3 style='background:#4CAF50;color:#FFFFFF;height:30px; width:650px; margin-bottom:5px; padding-top:5px;'align='center'> See  All Your Messages</h3>
 

<style>
table{border-collapse:collapse border:solid #d0d0d0 1px;}
th{border-collapse:collapse;border:white 3px;padding:2px;font-family:Tahoma, Geneva, sans-serif;}
table,td{padding:7px;}
td a {text-decoration:none; font-size:18px;}
td a:hover { color:brown;font-weight:bold;}
</style>
 
<p align="center">
<a href="messages.php?inbox">My Inbox</a> || 
<a href="messages.php?send">Sent Items</a>
</p>

<?php if(isset($_GET['send'])) {
	include("send.php");
}?>

<?php if(isset($_GET['inbox'])) {?>
<table border="1"  width="650"  bgcolor="green">
<tbody>
 <tr>
 <th bgcolor="#BDB76B">Sender </th>
 <th bgcolor="#BDB76B">Subject </th>
 <th bgcolor="#BDB76B">Date</th>
 <th bgcolor="#BDB76B">Reply</th>
 </tr>
 <?php

$select_msg ="select * from messages  where receiver = '$user_id'  ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);

while($row= mysqli_fetch_array( $run_msg)){	

$msg_id= $row['msg_id'];
$msg_receiver= $row['receiver'];
$msg_sender= $row['sender'];
$msg_sub= $row['msg_sub'];
$msg_topic= $row['msg_topic'];
$msg_date = $row['msg_date'];

$get_sender=" select * from users where user_id='$msg_sender' ";
$run_sender =mysqli_query($con,$get_sender);
$row_sender= mysqli_fetch_array( $run_sender);

$sender_name= $row_sender['user_name'];

	 
?>	
	<tr >
    <td>
	<a href="user_profile.php?u_id=<?php echo $msg_sender;?>"target="_self">
	<?php echo $sender_name;?></a>
    </td>
    <td>
    <a href="messages.php?inbox&msg_id=<?php echo $msg_id;?>">
    <?php echo $msg_sub;?></a>
    </td>
    <td><?php echo $msg_date;?></td>
    <td>
    <a href="messages.php?inbox&msg_id=<?php echo $msg_id;?>">Reply</a>
    </td>
   </tr>
   

  </tbody>
  <?php } ?>
</table>
       <?php
    
if(isset($_GET['msg_id'])){
	
$get_id=$_GET['msg_id'];

$sel_message="select * from messages where msg_id='$get_id'";
$run_message =mysqli_query($con,$sel_message);
$row= mysqli_fetch_array( $run_message);	

		 
$msg_subject= $row['msg_sub'];
$msg_topic= $row['msg_topic'];
$reply_content= $row['reply']; 

// updating the unread messages to read
$update_unread ="update messages set status='read' where msg_id='$get_id'";

$run_unread = mysqli_query ($con,$update_unread);
echo " <center><br/><hr width='500'>

<h2>$msg_subject</h2>
<span><p> <b>Message:</b>$msg_topic</p>
<p><b>My  Reply:</b>$reply_content</p>

<form action='' method='post' id='f'>
<textarea name='reply' cols='35' rows='5' placeholder='Message Reply.....'></textarea>  <br/>
<input type='submit' name='msg_reply' value='Reply Message' style='margin-top:10px;background-color: #4CAF50;'><br/>
</form>
<hr width='500'></span></center>
 ";
		}
		
if(isset($_POST['msg_reply'])){
	
$user_reply = addslashes($_POST['reply']);

if($reply_content!='no_reply'){
echo "<center><h3 style='background:#4CAF50; color:#FFFFFF; height:30px; width:470px; margin-bottom:5px; padding-top:5px;'  align='center'> Message was already replied !!</h3></center>";	
		exit();
		} else {
	$update_msg = "update messages set reply='$user_reply' where msg_id='$get_id'";
	$run_update = mysqli_query($con,$update_msg);

echo "<center><h3 style='background:#4CAF50; color:#FFFFFF; height:30px; width:470px; margin-bottom:5px; padding-top:5px;'  align='center'> Message was replied !!</h3></center>";

}
}
}?>
  </div></div></div></div>
</body>
</body>
</html>
<?php  } ?>