<?php
ob_start();
session_start();
include("includes/connection.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<html>
<head>
<meta charset="utf-8">

<link rel="stylesheet" href="styles/home_style3.css" media="all" />
<title>Farmers ChatBox</title>
</head>
<body>
<div class="container">
<div id="head_wrap">
<div id="header">
<h4 > Welcome :<?php  echo $_SESSION['user_email'];?></h4>
<ul id="menu">
<li><a href="home.php"> Home</a></li>
<li><a href="members.php"> Members</a></li>
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics)){
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic.php?topic=$topic_id '>$topic_title </a> </li>" ;
	}
?>
<form action="results.php" method="get" id="form1">
<input type="text" name="user_query" placeholder="search a topic" required="required" />
<input type="submit" name ="search" value="Search" />
</form>
</ul>
</div>

<div class="content">

<div id="user_timeline">

<div id="user_details"> 
 
 <?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];
 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
  $posts= mysqli_num_rows($run_posts); 
  
  //getting total number of messages
$select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 
 
echo " 
<img src='images/$user_image' width='200' height='200'  />
<p><strong> Name: </strong> $user_name </p>
<p><strong> Cellphone: </strong> $user_phone </p>
<p><strong> County: </strong> $user_county </p>
<p><strong> Last Login: </strong> $last_login </p>
<p> <a href='messages.php?inbox&u_id=$user_id'> My messages ($count_msg)</a></p>
<p> <a href='my_posts.php?u_id=$user_id'> My Posts ($posts)</a></p>
<p> <a href='edit.php?u_id=$user_id'> Edit Profile</a></p>
<p> <a href='logout.php'> Logout</a></p>
";
 
 ?>
 </div></div>
<div id="userss">
 <h3 style="background:#4CAF50; color:#FFFFFF; height:40px; width:220px; margin-bottom:5px; padding-top:5px;"  align="center"> All Registered Users :</h3>
<?php getUsers();?>
</div></div>
<div id="content_timeline">
<?php 
 if(isset($_GET['u_id']))
	{
		$u_id= $_GET['u_id'];
	    $sel ="select * from users where user_id ='$u_id'";
		$run_user =mysqli_query($con,$sel);
		$row= mysqli_fetch_array($run_user);
		
		$last_login= $row['last_login'];
		$user_name= $row['user_name'];
		$user_image= $row['user_image'];
	}
	?>
<h3 style="background:#4CAF50;color:#FFFFFF; height:40px; width:470px; margin-bottom:5px; padding-top:9px;" align="center"> Send a message to <span> <a style="text-decoration:none;" href='user_profile.php?u_id=$user_id'> <?php echo $user_name;?> </a></span></h3>

<form action="my_messages.php?u_id=<?php echo $u_id;?>" method="post" id="f">
<input type="text" name="msg_title" required="required" placeholder=" Message Subject" size="53" style="margin-bottom:10px;"> 
<textarea name="msg" cols="40" rows="5" required placeholder="Message Topic....."></textarea>  <br/>
<input type="submit" name="message" value="Send Message" style="margin-top:10px;background-color: #4CAF50;"><br/>
 </form>
<img src='images/<?php echo $user_image; ?>'width= '40' height ='40' style =' margin-right:10px;float:left; border:2px solid #4CAF50; border-radius:5px;'>
<p style ='margin-left:25px; margin-top:25px;'><strong><?php echo $user_name;?></strong> is a member on this site since: <?php echo $last_login;?></</p>
<?php
if(isset($_POST['message'])){
	global $con;
	$msg_title= addslashes($_POST['msg_title']);
	$msg= addslashes($_POST['msg']);
	
if($msg_title==''){
 echo "<h3 style='background:#4CAF50; color:#FFFFFF; height:30px; width:470px; margin-bottom:5px; padding-top:5px;'  align='center'> please enter message title:</h3>";
 exit();
		
}else{
$insert="insert into messages(sender,receiver,msg_sub,msg_topic,reply,status,msg_date) values ('$user_id','$u_id','$msg_title','$msg','no_reply','unread',NOW())";

$run= mysqli_query($con,$insert);
if($run){
echo "
<h3 style='background:#4CAF50; color:#FFFFFF;  height:30px; width:470px; margin-top:35px; margin-bottom:5px; padding-top:5px;'  align='center'>Message was sent to " .$user_name. " successfully</h3>";
header("Location: messages.php");
}
else { echo "
<h3 style='background:#4CAF50; color:red;  height:30px; width:470px; margin-top:35px; margin-bottom:5px; padding-top:5px;'  align='center'>Message was  not sent .....!!</h3>";}
}}	
?>



 
  </div></div></div></div>
</body>
</body>
</html>
<?php  } ?>