<!doctype html>
<?php
session_start();
include("includes/connection.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<html>
<head>
<meta charset="utf-8">

<link rel="stylesheet" href="styles/home_style.css" media="all" />
<title>Farmers ChatBox</title>
</head>
<body>
<div class="container">
<div id="head_wrap">
<div id="header">
<h4 > Welcome :<?php  echo $_SESSION['user_email'];?></h4>
<ul id="menu">
<li><a href="home.php"> Home</a></li>
<li><a href="members.php"> Members</a></li>
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics)){
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic.php?topic=$topic_id '>$topic_title </a> </li>" ;
	}
?>
<form action="results.php" method="get" id="form1">
<input type="text" name="user_query" placeholder="search a topic" />
<input type="submit" name ="search" value="Search" />
</form>
</ul>
</div>
<div id="userss">
<h3>All Registered Users :</h3>
</div></div>
<div class="content">
<div id="user_timeline">
<div id="user_details"> 
 
 <?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];

  $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 
echo " 
<img src='images/$user_image' width='200' height='200'  />
<p><strong> Name: </strong> $user_name </p>
<p><strong> Cellphone: </strong> $user_phone </p>
<p><strong> County: </strong> $user_county </p>
<p><strong> Last Login: </strong> $last_login </p>
<p> <a href='messages.php?inbox&u_id=$user_id'> My messages ($count_msg)</a></p>
<p> <a href='my_posts.php?u_id=$user_id'> My Posts ($posts)</a></p>
<p> <a href='edit.php'> Edit Profile</a></p>
<p> <a href='logout.php'> Logout</a></p>
";
 
 ?>
 </div>
 </div>
   <div id="content_timeline">
   
   <h3> Your Search Results</h3>
   <?php GetResults();?>
  
</div>
</div>
</div>
</div>
</body>
</body>
</html>
<?php  } ?>