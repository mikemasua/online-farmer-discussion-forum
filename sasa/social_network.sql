-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2017 at 10:08 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social_network`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_email`, `admin_pass`) VALUES
(1, 'admin@admin.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `comment_author` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `post_id`, `user_id`, `comment`, `comment_author`, `date`) VALUES
(78, 78, 13, 'Yes.....its possible\r\n\r\nYou have to get the right materials n do it', 'amina Abdi', '2017-01-10 08:18:37'),
(81, 81, 16, 'This is a very wide topic  fellow', 'jameshavoc', '2017-01-10 14:12:12'),
(82, 81, 16, 'But You guys you can help ,,...', 'jameshavoc', '2017-01-10 14:16:55'),
(83, 81, 16, 'yes tell them to assist me', 'Amina Abdi', '2017-01-10 14:17:44'),
(84, 82, 17, 'Look at online stores', 'Amina Abdi', '2017-01-10 14:22:53'),
(85, 81, 16, 'Yes...consider breaking it into parts', 'Joe Mucheru', '2017-01-11 07:37:04'),
(86, 85, 21, 'Where are u located so that i may pay  u a visit...', 'Tony Active', '2017-01-11 07:53:30'),
(87, 87, 17, 'red get me well\r\n', 'Amina Abdi', '2017-01-16 13:18:49'),
(88, 84, 20, 'You Can Buy ', 'Amina Abdi', '2017-01-16 15:32:33'),
(89, 92, 18, 'come inbox\r\n', 'Joe Mucheru', '2017-01-22 20:42:10'),
(90, 91, 22, 'i will get in touch  with you\r\n', 'Joe Mucheru', '2017-01-22 21:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `msg_id` int(100) NOT NULL,
  `sender` int(100) NOT NULL,
  `receiver` int(100) NOT NULL,
  `msg_sub` text NOT NULL,
  `msg_topic` text NOT NULL,
  `reply` text NOT NULL,
  `status` text NOT NULL,
  `msg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`msg_id`, `sender`, `receiver`, `msg_sub`, `msg_topic`, `reply`, `status`, `msg_date`) VALUES
(18, 17, 16, 'Hello Amina', 'How are you...', 'Am fine pal', 'read', '2017-01-22 19:01:54'),
(19, 18, 16, 'Greetings', 'Hey Aminaaa', 'Wat do you need', 'read', '2017-01-22 18:52:11'),
(20, 16, 18, 'Hi', 'see there', 'okay', 'read', '2017-01-22 18:50:21'),
(21, 17, 16, 'Concern on piglets', 'where are you located???', 'At cleveland ,south of tennesse', 'read', '2017-01-22 19:01:44'),
(22, 16, 18, 'hi', 'see there', 'okay...carry tools', 'read', '2017-01-22 19:43:26'),
(23, 16, 17, 'sasaaaaaaa', 'Sasa jemmo', 'YESS ....KANA KAAA NDAVYE', 'read', '2017-01-22 19:47:06');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `post_title` text NOT NULL,
  `post_content` text NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `user_id`, `topic_id`, `post_title`, `post_content`, `post_date`) VALUES
(81, 16, 1, 'Communal farming procedures ...discussion', 'Hey lets discuss this', '2017-01-10 13:17:31'),
(82, 17, 4, 'Hey community', 'Where can i  buy ploughing machines', '2017-01-10 14:11:28'),
(83, 20, 2, 'Diary farmin in Rural Areas', 'is it recommendable to do dairy farming in rural areas??', '2017-01-11 07:38:19'),
(84, 20, 4, 'Machines for sale', 'Where can i get tools for tilling land ?', '2017-01-11 07:39:21'),
(85, 21, 6, 'Chicken for sale', 'A selling chics at  cost of 250 kes', '2017-01-11 07:44:31'),
(86, 19, 0, 'Banana Seedlings For sale', 'Anybody selling banana seedling here???...Quote your price ', '2017-01-13 07:11:37'),
(87, 17, 6, 'Turkey eggs needed asap', 'If you are selling Turkey eggs around kongowea i need them', '2017-01-13 11:43:10'),
(88, 27, 2, 'Weekend Weather Forecast 19/1/17', 'It''s been a week of contrasts for many; cloud and mist for some, sunshine and chillier weather for others. The weekend looks very similar.\r\n\r\nThanks to all who have asked to be added to the waiting list for the next Weather School courses. I''ll be announcing dates soon, but if you''d like to be notified first of the dates', '2017-01-19 19:04:47'),
(89, 27, 2, ' Hurdles for lambing pens', 'In after some new hurdles to make lambing pens up with. Would like 5ft so they fit in the Polytunnel better, preferably wooden but can be metal.\r\nI want new ones, don''t have time to make my own before poeple start suggesting ideas. Lol!!\r\nBasically any ideas where I can get some???? ', '2017-01-19 19:55:06'),
(90, 27, 2, 'Artificial calf colostrum', 'I know the best answer is the real stuff but I''m in a corner where I need some artificial calf colostrum put by just in case (small number of cows in very remote location, no colostrum left in the freezer, the cow that had the dreadful summer mastitis is probably going to calve first ! :rolleyes:).\r\nSo if you had to buy the artificial stuff what make would you get & how much would you use?\r\nThanks. ', '2017-01-19 20:02:07'),
(91, 22, 5, 'Towing with an Amarok', 'is it really capable with only a 2.0 engine?\r\n\r\nLooking at a 180hp one, would be looking to tow up to 3 tonne some days.', '2017-01-20 00:07:04'),
(92, 18, 3, 'Piglets for sale', 'Am selling 2 piglets ...if  you need em ...inbox me', '2017-01-21 16:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL,
  `topic_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`topic_id`, `topic_title`) VALUES
(1, 'Crops'),
(2, 'Livestock'),
(3, 'Forestry'),
(5, 'Machinery'),
(6, 'Poultry');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_role` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_pass` varchar(100) NOT NULL,
  `user_phone` int(100) NOT NULL,
  `user_gender` varchar(100) NOT NULL,
  `user_county` varchar(100) NOT NULL,
  `user_image` text NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` text NOT NULL,
  `posts` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_role`, `user_name`, `user_email`, `user_pass`, `user_phone`, `user_gender`, `user_county`, `user_image`, `last_login`, `status`, `posts`) VALUES
(16, 'Please Select', 'Amina Abdi', 'amina@gmx.com', 'AMINA', 900000020, 'Female', 'Kitui', 'c16.jpg', '2017-01-16 13:28:17', 'unverified', 'yes'),
(17, 'Laboaratoligist', 'jameshavoc', 'jameshavoc@gmail.com', 'JAMESHAVOC', 2147483647, 'Male', 'kisumu', 'vlcsnap-2016-12-19-14h50m25s467.png', '2017-01-10 14:11:28', 'unverified', 'yes'),
(18, 'Vet.Officer', 'Alex Koome', 'alex@gmail.com', 'alexkoome', 900000022, 'Male', 'Meru', 'watsapp.PNG', '2017-01-22 10:01:17', 'unverified', 'yes'),
(19, 'Agri.Engineer', 'Tony Active', 'tony@yahoo.com', 'tonyactive', 900000023, 'Male', 'Msa', 'IMG-20161027-WA0010.jpg', '2017-01-11 07:54:36', 'unverified', 'yes'),
(20, 'Agri.Engineer', 'Joe Mucheru', 'joem@gmail.com', 'joemucheru', 900000025, 'Male', 'Eldoret', 'c15.jpg', '2017-01-16 13:08:49', 'unverified', 'yes'),
(21, 'TechSupport', 'Charity Ngilu', 'charity@gmail.com', 'charityngilu', 900000026, 'Female', 'Machakos', '3.jpg', '2017-01-11 07:44:31', 'unverified', 'yes'),
(22, 'Farmer', 'james Muimi', 'jamesmuimi@gmail.com', 'jamesmuimu', 2147483647, 'Male', 'Kitale', 'images.jpg', '2017-01-20 00:07:04', 'unverified', 'yes'),
(24, 'HumanResource', 'Chief Makau', 'chief011@gmail.com', 'chiefmarcos', 2147483647, 'Male', 'Kwale', 'crown.jpg', '2017-01-13 15:26:02', 'unverified', 'No'),
(27, 'Farmer', 'peter aamz', 'peter@gmail.com', 'peteradamz', 2147483647, 'Male', 'kisumu', 'c14.jpg', '2017-01-19 19:04:47', 'unverified', 'yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `msg_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
